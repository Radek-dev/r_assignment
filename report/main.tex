%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% fphw Assignment
% LaTeX Template
% Version 1.0 (27/04/2019)
%
% This template originates from:
% https://www.LaTeXTemplates.com
%
% Authors:
% Class by Felipe Portales-Oliva (f.portales.oliva@gmail.com) with template 
% content and modifications by Vel (vel@LaTeXTemplates.com)
%
% Template (this file) License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[
	12pt, % Default font size, values between 10pt-12pt are allowed
	%letterpaper, % Uncomment for US letter paper size
	%spanish, % Uncomment for Spanish
]{fphw}

% Template-specific packages
\usepackage[utf8]{inputenc} % Required for inputting international characters
\usepackage[T1]{fontenc} % Output font encoding for international characters
\usepackage{mathpazo} % Use the Palatino font

\usepackage{graphicx} % Required for including images

\usepackage{booktabs} % Required for better horizontal rules in tables

\usepackage{listings} % Required for insertion of code

\usepackage{enumerate} % To modify the enumerate environment

\usepackage{pdfpages} % To load the paper to this file.

\usepackage{bm} % For bold math symbols

\usepackage{amsmath} % For making symbols underneath math

\usepackage{hyperref} % For making links

\usepackage{enumitem} % For making enumation lists

\usepackage[title]{appendix} % For appendecis

%----------------------------------------------------------------------------------------
%	ASSIGNMENT INFORMATION
%----------------------------------------------------------------------------------------

\title{Assignment} % Assignment title

\author{Radek Chramosil} % Student name

\date{May 8th, 2020} % Due date

\institute{Birckbeck, University of London \\
		Department of Economics, Mathematics and Statistics} % Institute or school name

\class{MSc Applied Statistics: Statistical Learning} % Course or class name

\professor{Georgios Papageorgiou} % Professor or teacher in charge of the assignment

%----------------------------------------------------------------------------------------

\begin{document}

\maketitle % Output the assignment title, created automatically using the information in the custom commands above

%----------------------------------------------------------------------------------------
%	ASSIGNMENT CONTENT
%----------------------------------------------------------------------------------------

\section*{Question 1}

\begin{problem}
	Select, read and summarize a paper of your own interest. The paper has to be
	related to the material we covered in class. Submit the paper and a 2-3 page report
	that includes:

	\begin{itemize}
		\item a short summary of the paper,
		\item description of the study,
		\item description of the methods used,
		\item the paper’s findings;
		\item and anything else you think relevant.
	\end{itemize}
	Further, comment on anything you particularly liked or disliked about the paper.
\end{problem}

%------------------------------------------------

I will work with the paper \textbf{Predicting breast cancer survivability: a comparison
of three data mining methods} by \textit{Delen, Walker and Kadam} (2004). The paper is attached at
the end of this report in Appendix \ref{appx:paper}.

\subsubsection*{Short Summary of the Paper}
\paragraph{}
The objective of the paper was to develop predictive models for and explain the relationships
between independent explanatory variables and survivability of breast cancer. The work carried
out in the paper establishes three predictive methods, i.e. Neural Networks, Logistic Regression
and Decision Trees. In addition, the work shows the sensitivity of breast cancer survivability
with respect to a number of prognostic factors (meaning the explanatory variables) using the
Neural Network framework only. In addition, the methods' accuracy numbers based on 10-fold
cross-validation are provided.

\subsubsection*{Description of the Study}
\paragraph*{Data Used}
The dataset used in the paper consisted of, most importantly, one binary categorical dependent
variable, named survival, and of originally of 433,272 cases or records and 72 prognostic factors.
After cleaning and aggregating the data, the study was based on 202,932 records with 16 prognostic
factors. So, 17 variables in total.
\paragraph*{}
The main idea of the data cleaning was to keep the distributional properties of the variable
consistent (before and after cleaning) when dealing with issues such as missing data. About a
half of the cases was dropped due to problems with the records.
\paragraph*{}
The important variable to mention is the survival variable. The survival was defined as being alive
after 5 years from the initial diagnosis. Hence, "1" for survived and "0" for did not survive.
The survival variable had the following distribution; 54\% did not survive and 46\% survived.
It should be noted that this distribution may change based on how we define the variable, so it seems
that there is a degree of subjectivity at play here.
\paragraph*{}
The prognostic factors are listed in Table \ref{table:1}. It lists some properties of the explanatory
dataset.

\begin{table}[h!]
	\centering
	\small
	\begin{tabular}{l c c c} 
	\toprule
	Variable Name & Variable Type & Number of Categories & Range \\ [0.5ex] 
	\midrule
	Race 						& categorical	& 28	& NA		\\
	Marital Status	 			& categorical	& 6		& NA		\\
	Primary site code 			& categorical 	& 9		& NA		\\
	Histology 					& categorical 	& 91	& NA		\\
	Behaviour 					& categorical 	& 2		& NA		\\ 
	Grade 						& categorical 	& 5		& NA		\\ 
	Extension of disease 		& categorical 	& 29	& NA		\\ 
	Lymph node involvement		& categorical 	& 10	& NA		\\ 
	Radiation					& categorical 	& 10	& NA		\\ 
	Stage of cancer 			& categorical 	& 5		& NA		\\ 
	Site specific surgery code	& categorical 	& 1		& NA		\\
	Age							& continuous	& NA	& 10—106	\\ 
	Tumor size					& continuous	& NA	& 0—200		\\
	Number of positive nodes	& continuous	& NA	& 0—75		\\
	Number of nodes				& continuous	& NA	& 0—91		\\
	Number of primaries			& continuous	& NA	& 1—8		\\ 
	[1ex] 
	\bottomrule
	\end{tabular}
	\caption{Prognostic Factors}
	\label{table:1}
\end{table}

It should be noted that the continuous variables should have been or, at least, could have
been standardised as, for example, Number of primaries does not scale well with Age.

\paragraph*{Nerual Network} The Nerual Network that was used in this paper can be described
using our notation with the below equations \ref{eq:hidden_layer} to \ref{eq:final}.

\begin{problem}
	\begin{equation}\label{eq:hidden_layer}
		z_{j} = \sigma(\bm{\alpha}_j^T \bm{x}_i) 
					, \quad j = 1,...,k \quad \textnormal{and} \quad i = 1,...,n 
	\end{equation}
	\begin{equation}\label{eq:output_layer}
		t = \bm{\beta}^T  \bm{z}
	\end{equation}
	\begin{equation}\label{eq:final}
		\hat{y} = g(t)
	\end{equation}
\end{problem}

where $\bm{x}_i$ is $p \text{x} 1$ vector with $p$ being a very large number due to the
presence of a number of categorical variables. I included an additional variable with
the value one in this term. This variable is needed for the bias term in this vector.
$\bm{\alpha}_j^T$ is $1 \text{x} p$ where $p$ matches the number of explanatory 
variables in $\bm{x}_i$. $j=15$ based on what is written in the paper, so the size of
$\bm{z}$ is $16 \text{x}1$, including the bias term $z_0$. The vector $\bm{\beta}^T$ 
is of $1\text{x}16$. As $p$ carries few hundred parameters and we have 15 + 1 nodes in
the hidden layer, the total number of parameters comes in thousands. This seems large.

It is not fully indicated in the paper which non-linear function was used as the
activation function $\sigma(\cdot)$. Similarly, function $g(\cdot)$ is not discribed in any way.
Further, the fitting method was gradient decent with 10-fold cross validation, however
it was not indicated, whether they kept the learning $\gamma$ constant or if any update
algorithm for $\gamma$ was used. Similarly, no idication for for the cost function or
weight decay parameter $\lambda$ was given.

\paragraph*{Logistic Regression} This is a binary classification problem, so Logistic
Regression is relevant. This method for this case might be summarised as

\begin{problem}
\begin{equation}\label{eq:logistic_regression}
	\hat{\pi}(\bm{x}_i) = \frac{exp(\bm{x}_i^T \bm{b})}{1 + exp(\bm{x}_i^T \bm{b})} \quad
	\textnormal{ with } i = 1,...,n  \textnormal{.}
\end{equation}
\end{problem}

As previously discussed, vectors $\bm{x} \text{ and } \bm{b}$ are large given the dataset.
Perhaps the most strinking difference to the neural network is that this method assumes
linear relationships between the covariates (as well as classes within the covariates),
as discussed in the paper. There is only one non-linear relationship. This has a standard
MLE estimation method based on a binomial log-likelihood.

\paragraph*{Decition Trees} The authors used C5 algorithm after a few test runs on different
algorithms. C5 is a largely undocumented algorithm, only the source code was
made public. A good description is provided Kuhn and Johnson, Applied Predictive Models
(2013). C5 algorithm is an improved C4.5 algorithm.

C5 algorithm uses entropy and information gains to build the tree. The formulas for this
are identical to what we have used in the lectures. Pruning is carried out with a parameter
called confidence factor and the logic is that the larger the value it has, the greater the tree.
However, there is no other theoretical validation of the confidence factor parameter, but only
the accuracy of the prediction of tree with different sizes. No matter which algorithm we
look at (C4.5, C5 or \textit{rpart} in R), they all seem to be driven by the accuracy of
prediction they can produce as well as the bias-variance trade-off. This applies to the
tree selection too. However, there are many differences in the details these algorithms have.

It rather seems that the authors of this paper did not investigate the exact details of
the C5 algorithm, most likely because they focused on the accuracy of the model rather
than the building and prunning. 


\subsubsection*{Paper's Findings}
\paragraph{Accuracy}
The main findings can be summaries in Table \ref{table:2}. The accuracy numbers are the
averages over 10-fold cross-validation.

\begin{table} [h!]
	\centering
    \begin{tabular}{lc}
    Model                        & Accuracy \\ \hline
    Decision Tree - C5 algorithm & 93.6\%   \\
    Neural Network               & 92.1\%   \\ 
    Logistic Regression          & 89.2\%   \\
	\end{tabular}
	\caption{Model Accuracy}
	\label{table:2}
\end{table}

Further, the authors of the paper used Sensitivity and Specificity to measure the model
fit, however, considering the numbers, they presented in Table 3, both of these measures
lead to the same conclusions as Accuracy. For this reason I present Accuracy only here.

\paragraph*{Sensitivity Analysis} Sensitivity analysis was carried out with the aim of
extracting the cause and effect relationship between the covariates and the dependent
variable. This involves changing an input variable by defined standard deviations
while keeping the parameters and other input variables constant. The Sensitivity is
then the percentage change in the output. Thus, the most important variables are the
ones with the greatest impact on the dependant variable. These variables are
then thought to  have the strongest causal relationship to the target variable. 
This Sensitivity analysis was done only for Neural Networks and revealed that Grade and 
Stage of Cancer were the most relevant prognostic factors, which made intuitive sense.

\subsubsection*{Other Comments}
\paragraph*{}
I find that the section on Neural Networks misses out on a lot of details that are relevant
to the structure of the network. This might be because the paper is quite old now.
The Decision Tree section in the paper felt not detailed enough for a journal paper.

Sensitivity analysis also feels like a missed opportunity. It would be interesting to
see if other methods (Decision Tree or Logistic Regression) would lead to same or similar 
factors as being relevant or if these methods would hint to conflicting decisions. 

Further, the paper does not hint how to apply these methods to actual live situations.
I felt that if they would try to apply the methods that way, they would handle the
data cleaning in a different manner. It is not very clear how the authors would use the
models if they had a new patient with incomplete dataset.

It can be noted that the authors made a typo in the number of neurons in the graphic
representation of the neural network, however this is not very important.
%----------------------------------------------------------------------------------------

\section*{Question 2}
\renewcommand{\thesection}{\Roman{section}}
\renewcommand{\thesubsection}{\thesection.\Roman{subsection}}



\begin{problem}
Analyze a dataset of your own interest using supervised learning methods. Datasets can
be found within the R packages discussed in class or in the UCI machine learning  repository
\href{http://archive.ics.uci.edu/ml/index.php}{at this link}. However,  you  are not restricted
to those datasets.

\

\begin{itemize}
	\item Describe the data in detail.
	\item Describe the model you used (including the model formulas).
	\item Provide your findings.
\end{itemize}

Illustrate that you have understood and  that  you  know  how  to  run  in  R
at least 2 methods. It is not necessary to consider more than 2 methods.
\end{problem}

%------------------------------------------------

\section{Data Description}
I used Students' Academic Performance Dataset from Kaggle website available
\href{https://www.kaggle.com/aljarah/xAPI-Edu-Data}{here}
\footnote{https://www.kaggle.com/aljarah/xAPI-Edu-Data}. There are 16 covariates and
the target is a 3-class ordinal variable. The overview of the variables is presented
in Table \ref{table:3}. Figures \ref{figure:01} to \ref{figure:03} show the
distributional properties of each variable. The plots were created using R code as
given in Appendix \ref{appx:R_plotting}. The related paper is Amrieh, et al., 2016,
\textit{Mining Education Data To Predict Student's academic performance using
Ensamble Methods}.

I provide specific definitions to each variable here. The definitions are taken from the Kaggle
website.

\begin{enumerate} [label=\arabic*)]
	\item \textit{Gender} - student's gender
	\item \textit{Nationality} - student's nationality (’Kuwait’, ’Lebanon’, ’Egypt’, ’SaudiArabia’, ’USA’, 
		’Jordan’, ’Venezuela’, ’Iran’, ’Tunis’, ’Morocco’, ’Syria’, ’Palestine’, ’Iraq’, ’Lybia’)
	\item \textit{Place of birth} - student's Place of birth (the same countries as in Nationality)		
	\item \textit{Stages ID} - educational stage/level student belongs to (‘lower level’, ’Middle
	\\School’, ’High School’)
	\item \textit{Grade ID} - grade level student belongs to (‘G-01’, ‘G-02’, ‘G-03’, ‘G-04’, ‘G-05’,
		‘G-06’, ‘G-07’, ‘G-08’, ‘G-09’, ‘G-10’, ‘G-11’, ‘G-12‘)
	\item \textit{Section ID} - classroom student belongs (’A’,’B’,’C’)
	\item \textit{Topic} - course topic (’English’, ’Spanish’, ‘French’, ’Arabic’, ’IT’,
		’Math’, ’Chemistry’, ‘Biology’, ‘Science’, ’History’, ’Quran’, ’Geology’)
	\item \textit{Semester} - school year semester (’First’, ’Second’)
	\item \textit{Relation} - Parent responsible for student (’mom’,’father’)
	\item \textit{Raised hand} - how many times the student raises his/her hand in the classroom
	\item \textit{Visited resources} - how many times the student visits a course content
	\item \textit{Viewing announcements} - how many times the student checks the new announcements
	\item \textit{Discussion groups} - how many times the student participate on discussion groups
	\item \textit{Parent Answering Survey} - parent answered the surveys which are provided from
		school or not (’Yes’,’No’)
	\item \textit{Parent School Satisfaction} - degree of parent satisfaction from school (’Yes’,’No’)
	\item \textit{Student Absence Days} -he number of absence days for each student (above-7, under-7)
\end{enumerate}

\begin{table} [!h]
	\centering
	\scriptsize
	\begin{tabular}{l c l c c}
	\toprule
	Variable Name & Dependent/Explanatory & Variable Type & Number of Categories & Range \\ [0.5ex] 
	\midrule
	Nationality		 			& explanatory	& categorical			& 14	& NA		\\
	Place of birth	 			& explanatory	& categorical			& 14	& NA		\\
	Topic				 		& explanatory	& categorical			& 12	& NA		\\ 
	Grade ID 					& explanatory	& ordinal				& 10	& NA		\\ 
	Stage ID 					& explanatory	& ordinal				& 3		& NA		\\
	Section ID					& explanatory	& categorical			& 3		& NA		\\ 
	Gender 						& explanatory	& categorical			& 2		& NA		\\
	Semester					& explanatory	& categorical			& 2		& NA		\\ 
	Relation					& explanatory	& categorical			& 2		& NA		\\ 
	Raised hands 				& explanatory	& discrete - count		& NA	& 0-100		\\ 
	Visited Resources			& explanatory	& discrete - count		& NA	& 0-100		\\
	Announcements View			& explanatory	& discrete - count		& NA	& 0-100		\\ 
	Discussion					& explanatory	& discrete - count		& NA	& 0-100		\\
	Parent Answering Survey		& explanatory	& categorical			& 2		& NA		\\
	Parentschool Satisfaction	& explanatory	& categorical			& 2		& NA		\\
	Student Absence Days		& explanatory	& ordinal				& 2		& NA		\\
	Class						& dependent		& ordinal				& 3		& NA		\\
	[1ex] 
	\bottomrule
	\end{tabular}
	\caption{Data Overview}
	\label{table:3}
\end{table}

\pagebreak

The \textbf{target variable} is \textit{Class}, which is a grade or mark with 3 categories:

\begin{itemize}
	\item Low-Level: interval includes values from 0 to 69,
	\item Middle-Level: interval includes values from 70 to 89,
	\item High-Level: interval includes values from 90-100.
\end{itemize}


Further, the below information was directly taken from (or shortened based on) the website and
it provides factual information about the dataset.

The dataset is collected using a learner activity tracker tool in an electronic
learning system, which allows for monitoring of learning progress and learner’s 
actions like reading an article or watching a training video.

The dataset consists of 480 student records. The features are classified into three
major categories: (1) Demographic features such as gender and nationality. (2) Academic
background features such as educational stage, grade Level and section. (3) Behavioural
features such as raised hand in class, opening resources, answering survey by parents,
and school satisfaction.

The dataset is collected through two educational semesters: 245 student records are collected
during the first semester and 235 student records are collected during the second semester.

The dataset includes also the school attendance feature such as the students are classified
into two categories based on their absence days: 191 students exceed 7 absence days and 289
students their absence days under 7.

This dataset includes also a parent participation in the educational process. The parent
participation feature has two sub features: Parent Answering Survey and Parent School
Satisfaction. There are 270 parents that answered the survey and 210 that did not, 292 of the
parents are satisfied with the school and 188 are not.

\vspace{10pt}

Turning the attention to the distribution properties of the variables as shown by the
Figures \ref{figure:01} to \ref{figure:03}, I note that the target variable is a clear
case of an ordinal variable with 44\% cases in the Middle-Level academic performance.
There is a lot of symmetry in this variable with 26\% and 30\% in Low-Level and
High-Level performance, see Figure \ref{figure:02}. It seems that this variable was
intentionally set up this way, considering that underlying class range spans from 0 to 100.

Considering Figure \ref{figure:01}, we see 4 features with varied classes. \textit{Nationality},
\textit{Place of Birth} and \textit{Grade ID} have a few extremas with other class values not 
being significantly present. For this reason, there is perhaps a bias present in these three
classes. This means that, if we would have a new student from Venezuela, the model should be
applied with caution. On the other hand, \textit{Topic} seems to be more evenly split over
the sample.

Figure \ref{figure:02} shows categorical variables with 2 or 3 classes. 
There is a time dimension to the data as we have two semesters here.  We could use this
aspect of dataset for forecast. For instance, we could use the probabilities of the target
\textit{Class} from the first semester as priors in a decision tree algorithm.
Additionally, we could use the \textit{Class} variable from the first semester as an explanatory
variable for the second semester. 

Looking at Figure \ref{figure:03}, \textit{Announcements Views} and \textit{Discussion Group}
may be possibly coming from Poison distribution as the values are counts and there is only one
peak in the data. However, \textit{Raised Hands} and \textit{Visited Resources} have two peaks
and so we perhaps would not be able to find standard distribution that would fit here. We could
consider these as an aggregation of two distributions.

Figure \ref{figure:04} shows the correlation for the four discrete variables. We should look
out for  the strong correlation as this is relevant for modelling the dataset.

To sum up, we should say that this problem has fairly high complexity as we are mapping a number
of explanatory variables with varied distributional properties and possible biases into what
can be seen as a single draw from one multinomial distribution with three outcomes. The best
solution is the most fitting mapping.

\begin{center}
	\begin{figure} [!h]
	\includegraphics[width=1\columnwidth]{Rplot01.png}
	\caption{Four Covariates}
	\label{figure:01}
	\end{figure}
\end{center}
\pagebreak
\vspace{-50pt}
\begin{center}
	\begin{figure} [!h]
	\includegraphics[width=1\columnwidth, height=0.8\textheight]{Rplot02.png}
	\caption{Covariates: Gender, Semester, Parent school Satisfaction,
			 Stage ID, Relation, Student Absence Days, Section ID and
			 Parent Answering Survey. Target: Class}
	\label{figure:02}
	\end{figure}
\end{center}

\vspace{-50pt}

\begin{center}
	\begin{figure} [!h]
	\includegraphics[width=1\columnwidth, height=0.5\textheight]{Rplot03.png}
	\caption{Covariates: Nationality, Place of Birth, Topic and Grade ID}
	\label{figure:03}
	\end{figure}
\end{center}

% \vspace{-35pt}

\begin{center}
	\begin{figure} [!h]
	\includegraphics[width=1\columnwidth]{Rplot04.png}
	\caption{Correlation: Nationality, Place of Birth, Topic and Grade ID}
	\label{figure:04}
	\end{figure}
\end{center}

\pagebreak


\section{Cumulative Logit Model for Ordinal Responses}

\paragraph*{Model Used}I have opted for Cumulative Logit Model. The reason for this choice is that the
output variable is ordinal. The model relies on the cumulative probabilities

\begin{problem}
	\begin{equation}\label{eq:cumulative_logit_prob}
		P(Y \leq k|\bm{x}_i) = \pi_1(\bm{x}_i) + \dots + \pi_j(\bm{x}_i),
		\quad i = 1,...,n.
	\end{equation}
\end{problem}

$P(Y \leq k|\bm{x}_i)$ represents the cumulative probability that the class $k$
or a lower class will occur given the dependent variables  vector $p \text{x} 1$
vector $x_i$. The fitted model is

\begin{problem}
	\begin{equation}\label{eq:cumulative_logit_fit}
		logit(P(Y \leq k|\bm{x}_i)) = \alpha_k - \bm{\beta}^T \bm{x}_i,
			\quad k=1,2.
	\end{equation}
\end{problem}

where $\alpha_k$ are the 2 intercepts as there are 3 categories. $\bm{\beta}$
is $p \text{x} 1$ vector with the number of input variables $p$,
which changes depending on which variables we consider in the model.
I used $logit$ link function as it was the best fitting the dataset by trail and
error. The minus sign is used to match how R fits the model.

\paragraph*{Model Fit} When fitting the model, I realised that four variables have too many categories
for the number of input variables. These variables are (\textit{Nationality},
\textit{Place of Birth}, \textit{Topic}, \textit{Grade ID}). I excluded them from the model
for practical reasons, however the correct solution would be to transform or aggregate them to
variable/s with less categories (such as using continent instead of country).

For the other variables I run the cumulative logit model multiple times starting with
the model with all the variables and removing the variables that were not statistically
significant (t-values smaller than 2), while minimising the AIC. The following variables
were not fitting well into the model: \textit{Stage ID}, \textit{Semester},
and \textit{Parentschool Satisfaction} and \textit{Section ID}. Some of them make
intuitive sense.

\textit{Announcements View} are fairly correlated with \textit{Raised Hands} and
\textit{Visited Resources} and somehow they came as the weakest explanatory variable
based on the t-test in the presence of the other variables. \textit{Announcements View}
variable was removed. My model was therefore reduced to

\begin{table} [!h]
	\centering
	\scriptsize
	\begin{tabular}{l c c c }
	\toprule
	Coefficients & Value & Std. Error & t-value \\ [0.5ex] 
	\midrule
	Raided Hands			 		&  0.0249	  & 0.00522			&  4.76	\\
	Visited Resources				&  0.0281	  & 0.00517			&  5.44	\\
	Discussion						&  0.0086	  & 0.00433			&  1.99	\\ 
	gender: M	 					& -0.6568	  & 0.23856			& -2.75	\\
	Relation: Mum	 				&  1.0277	  & 0.24749			&  4.15	\\
	Student Absence Days: Above-7	& -2.9764	  & 0.32894			& -9.05	\\
	Parent Answering Survey: Yes	&  1.2473	  & 0.24615			&  5.07	\\
	[1ex] 
	\bottomrule
	\end{tabular}
	\caption{Coefficients}
	\label{table:4}
\end{table}

\begin{table} [!h]
	\centering
	\scriptsize
	\begin{tabular}{l c c c }
	\toprule
	Intercepts & Value & Std. Error & t-value \\ [0.5ex] 
	\midrule
	L|M	 	& -0.177	  & 0.411			& -0.431	\\
	M|H		&  5.099	  & 0.522			&  9.761	\\
	[1ex] 
	\bottomrule
	\end{tabular}
	\caption{Intercepts}
	\label{table:5}
\end{table}

The Residual Deviance and AIC were 518.6794 and 536.6794, respectively.
This model is fitting well.

\paragraph*{Model Interpretation} We can interpret the model now. I calculated the classification probabilities
for one specific case. See the R code in Appendix \ref{appx:R_cumulative_logit}.
I used the Equation \ref{eq:cumulative_logit_prob}, however, we use the estimated
probabilities as in

\begin{problem}
	\begin{equation}\label{eq:cumulative_logit_fit}
		\hat{P}(Y \leq k) = \frac{exp(\hat{\alpha}_k - \hat{\bm{\beta}}^T \bm{x}_i)}
						{1 + exp(\hat{\alpha}_k - \hat{\bm{\beta}}^T \bm{x}_i)}.
	\end{equation}
\end{problem}

Our coefficients were estimated with a negative sign, so we have to use the
negative sign again.
My example calculation consisted of using medians for all discrete variables
used in the model and switching all categorical variables to 1. This has given me
the probablity $\hat{P}(L < Y \leq M) = 85.72\%$. This makes intuitive sense, as
this student has average activity as measured by the discrete variables, so he
has a high probability of being in the Middle Performance Level.

To sum up this section, it is worth saying that the model fit almost feels like
a textbook example. This was unexpected as usually, the real data is more difficult
than a textbook. I was surprised how well this model works for the dataset. The
weakness of this method is that it completelly fails to fit if we have more
categories than variables in the dataset.

\section{Decision Tree}
After trying to find best fitting tree using the \textit{rpart} algorithm, it seems
that the biggist problem any decision tree will have is that the algorithm gives 
many varied fits based on the small details. There is a large variability in the fits.

\vspace{-20pt}

\begin{center}
	\begin{figure} [!h]
	\includegraphics[width=1\columnwidth, height=0.4\textheight]{Rplot05.png}
	\caption{Decision Tree Fit}
	\label{figure:05}
	\end{figure}
\end{center}


The best tree I could find was by fitting all explanatory variables, including the ones
where we do not know if they are or are not relevant to the fit. I then prunned the tree
using the $cp=0.024$. I checked if fitting the tree with entropy would make any
difference and it did not. So, my decision tree is built with gini index. It is
plotted in Figure \ref{figure:05}. The full tree interpretation, in-sample
confusion matrix and R fitting is provided in Appendix \ref{appx:R_decision_tree}.


This fit is reasonable as the minimum decision probabilities across all leafs
were $2/3$. Otherwise, it is fairly clear what decision to make at each level
as judged by the highest probabilities at each leaf. Plot in Figure
\ref{figure:05} shows the generalisation error of the fit and it shows us that
the fit is correct in a sense that we reach the optimal point in the
generalisation error or, equivalently, in the bias-variance trade off for the
fit. This plot is also important when pruning the tree. We can use it to
decide on the \textit{cp} value.

\begin{center}
	\begin{figure} [!h]
	\includegraphics[width=1\columnwidth, height=0.4\textheight]{Rplot06.png}
	\caption{Decision Tree - Generalisation Error}
	\label{figure:06}
	\end{figure}
\end{center}

\vspace{-30pt}

To sum up, decision trees are easy to fit and once fitted, we can interpret them
easily. The problems are that we are unsure if some of the decision variables are
relevant. For instance, I tried to remove \textit{Discussion} from the fitting
process, as this variant never came up as being used. However, suddenly the
fitting algorithm was finding different nodes. So, I am not sure if
\textit{Discussion}  is relevant to the fit, though it impacts the building process.
We have input variables, that are not used in the fit, but still may have possible
impact on the build. The algorithm feels fairly unstable and it is not easy to
determine which variables are really needed and which are redundant.

\pagebreak

%-----------------------------------------------------------------------------------
\begin{appendices}

\section{Appendix - R Code for Cumulative Logit}\label{appx:R_cumulative_logit}

\lstinputlisting[
		language=R,
		caption=Cumulative Logit, % Caption above the listing
		numbers=left,
		breaklines=false,
		basicstyle=\footnotesize, 
		caption={Cumulative Logit Model},
		frame=single,
		numberstyle=\tiny,
	]{../data_analysis/cumulative_logit.R}

\section{Appendix - R Code for Decision Tree}\label{appx:R_decision_tree}

	\lstinputlisting[
			language=R,
			caption=Cumulative Logit, % Caption above the listing
			numbers=left,
			breaklines=false,
			basicstyle=\footnotesize, 
			caption={Decision Tree},
			frame=single,
			numberstyle=\tiny,
		]{../data_analysis/decision_tree.R}

\section{Appendix - R Code for plotting}\label{appx:R_plotting}


\lstinputlisting[
		language=R,
		caption=Plotting in R, % Caption above the listing
		numbers=left,
		breaklines=false,
		basicstyle=\footnotesize, 
		caption={Plotting in R},
		frame=single,
		numberstyle=\tiny,
	]{../data_analysis/data_analysis.R}

%----------------------------------------------------------------------------------------
\section{Appendix - The Paper for Question 1}\label{appx:paper}
The paper of my choice is attached below.

\includepdf[pages=-]{mypaper.pdf}


\end{appendices}

\end{document}
